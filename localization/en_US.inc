<?php
$labels['ac_title'] = 'Auto Cleaning';
$labels['ac_titleDest'] = 'Cleared mail';

$labels['ac_destTrash'] = 'move to Trash';
$labels['ac_destRemove'] = 'delete';

$labels['ac_afterN'] = 'after %s days';

$labels['ac_after1'] = 'after 1 day';
$labels['ac_after2'] = 'after 2 days';
$labels['ac_after3'] = 'after 3 days';
$labels['ac_after7'] = 'after 1 week';
$labels['ac_after14'] = 'after 2 weeks';
$labels['ac_after30'] = 'after 1 month';
$labels['ac_after60'] = 'after 2 months';
$labels['ac_after90'] = 'after 3 months';
$labels['ac_after180'] = 'after half year';
$labels['ac_after365'] = 'after one year';
$labels['ac_after730'] = 'after 2 years';
$labels['ac_after1826'] = 'after 5 years';

?>