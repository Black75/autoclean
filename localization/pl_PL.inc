<?php
$labels['ac_title'] = 'Automatyczne czyszczenie';
$labels['ac_titleDest'] = 'Czyszczone wiadomości';

$labels['ac_destTrash'] = 'przenieś do kosza';
$labels['ac_destRemove'] = 'usuń całkowicie';

$labels['ac_afterN'] = 'po %s dniach';

$labels['ac_after1'] = 'po 1 dniu';
$labels['ac_after2'] = 'po 2 dniach';
$labels['ac_after3'] = 'po 3 dniach';
$labels['ac_after7'] = 'po tygodniu';
$labels['ac_after14'] = 'po 2 tygodniach';
$labels['ac_after30'] = 'po miesiącu';
$labels['ac_after60'] = 'po 2 miesiącach';
$labels['ac_after90'] = 'po 3 miesiącach';
$labels['ac_after180'] = 'po pół roku';
$labels['ac_after365'] = 'po roku';
$labels['ac_after730'] = 'po 2 latach';
$labels['ac_after1826'] = 'po 5 latach';


?>
